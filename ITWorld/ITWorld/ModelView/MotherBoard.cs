﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITWorld.Models;

namespace ITWorld.ModelView
{
    public class MotherBoard
    {
        public Product product { get; set; }
        public Product_MotherBoard motherBoard { get; set; }
    }
}