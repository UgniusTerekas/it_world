﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITWorld.Models;

namespace ITWorld.ModelView
{
    public class Strips
    {
        public Product product { get; set; }
        public Product_Strips strips { get; set; }
    }
}