﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ITWorld.Models;

namespace ITWorld.ModelView
{
    public class OrderViewInformation
    {
        [DisplayName("Užsakymo numeris")]
        [Required]
        public string OrderID { get; set; }
        [DisplayName("Apmokėjimo būdas")]
        [Required]
        public string PaymentWay { get; set; }
        [DisplayName("Visa bendra suma")]
        [Required]
        public double Sum { get; set; }
        [DisplayName("Užsakymo data")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime orderDate { get; set; }
        [DisplayName("Užsakymo būsena")]
        [Required]
        public string Status { get; set; }
        public List<ShopingCart> cart { get; set; }
    }
}