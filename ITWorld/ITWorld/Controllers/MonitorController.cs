﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class MonitorController : Controller
    {
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_Monitor> monitors = new List<Product_Monitor>();
            monitors = db.Product_Monitor.ToList();
            if(monitors.Count > 0)
            {
                int min = (int)monitors.Min(c => c.Product.Price);
                int max = (int)monitors.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(monitors);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Monitor> monitors = new List<Product_Monitor>();
            monitors = db.Product_Monitor.ToList();
            if (monitors.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)monitors.Min(c => c.Product.Price);
                int masterMax = (int)monitors.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(monitors);
            }

            return View(monitors.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_Monitor.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}