﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class MotherboardController : Controller
    {
        // GET: Motherboard
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_MotherBoard> motherboards = new List<Product_MotherBoard>();
            motherboards = db.Product_MotherBoard.ToList();
            if (motherboards.Count > 0)
            {
                int min = (int)motherboards.Min(c => c.Product.Price);
                int max = (int)motherboards.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(motherboards);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_MotherBoard> motherboards = new List<Product_MotherBoard>();
            motherboards = db.Product_MotherBoard.ToList();
            if (motherboards.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)motherboards.Min(c => c.Product.Price);
                int masterMax = (int)motherboards.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(motherboards);
            }

            return View(motherboards.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_MotherBoard.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}