﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class CPUController : Controller
    {
        // GET: CPU
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_CPU> cpus = new List<Product_CPU>();
            cpus = db.Product_CPU.ToList();
            if (cpus.Count > 0)
            {
                int min = (int)cpus.Min(c => c.Product.Price);
                int max = (int)cpus.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(cpus);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Case> cpus = new List<Product_Case>();
            cpus = db.Product_Case.ToList();
            if (cpus.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)cpus.Min(c => c.Product.Price);
                int masterMax = (int)cpus.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(cpus);
            }

            return View(cpus.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_CPU.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}