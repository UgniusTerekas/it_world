﻿using ITWorld.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITWorld.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        Entities db = new Entities();
        public ActionResult Index(string searching)
        {
            return View(db.Product.Where(x => x.Name.Contains(searching) || searching == null).ToList());
        }

        public ActionResult ViewInfo(int? id)
        {
            Product product = db.Product.Where(x => x.ID == id).FirstOrDefault();

            if(product.Product_Graphics_Card != null)
            {
                int? prID = product.Product_Graphics_Card.ID;
                return RedirectToAction("ViewInfo", "GPU", new { id = prID });
            }
            else if (product.Product_Case != null)
            {
                int? prID = product.Product_Case.ID;
                return RedirectToAction("ViewInfo", "Case", new { id = prID });
            }
            else if (product.Product_Cooler != null)
            {
                int? prID = product.Product_Cooler.ID;
                return RedirectToAction("ViewInfo", "Cooler", new { id = prID });
            }
            else if (product.Product_CPU != null)
            {
                int? prID = product.Product_CPU.ID;
                return RedirectToAction("ViewInfo", "CPU", new { id = prID });
            }
            else if (product.Product_Headphones.Count != 0)
            {
                int? prID = product.Product_Headphones.First().ID;
                return RedirectToAction("ViewInfo", "Headphone", new { id = prID });
            }
            else if (product.Product_Keyboard.Count != 0)
            {
                int? prID = product.Product_Keyboard.First().ID;
                return RedirectToAction("ViewInfo", "Keyboard", new { id = prID });
            }
            else if (product.Product_Monitor.Count != 0)
            {
                int? prID = product.Product_Monitor.First().ID;
                return RedirectToAction("ViewInfo", "Monitor", new { id = prID });
            }
            else if (product.Product_MotherBoard != null)
            {
                int? prID = product.Product_MotherBoard.ID;
                return RedirectToAction("ViewInfo", "Motherboard", new { id = prID });
            }
            else if (product.Product_Mouse.Count != 0)
            {
                int? prID = product.Product_Mouse.First().ID;
                return RedirectToAction("ViewInfo", "Mouse", new { id = prID });
            }
            else if (product.Product_MousePad.Count != 0)
            {
                int? prID = product.Product_MousePad.First().ID;
                return RedirectToAction("ViewInfo", "MousePad", new { id = prID });
            }
            else if (product.Product_PSU != null)
            {
                int? prID = product.Product_PSU.ID;
                return RedirectToAction("ViewInfo", "PSU", new { id = prID });
            }
            else if (product.Product_RAM != null)
            {
                int? prID = product.Product_RAM.ID;
                return RedirectToAction("ViewInfo", "RAM", new { id = prID });
            }

            return RedirectToAction("Index","Home");
        }
    }
}