﻿using ITWorld.Models;
using ITWorld.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITWorld.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        Entities1 db = new Entities1();
        Entities db2 = new Entities();
        OrderModelEntity db3 = new OrderModelEntity();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateOrder()
        {
            if (Session["IdSession"] == null)
            {
                ViewBag.LoginErr = "Prašome prisijungti, jeigu norite atlikti praeitą operaciją";
                return RedirectToAction("Error", "Home");
            }

            OrderEditView order = new OrderEditView();

            List<Payment> paymentsMethods = db3.Payments.ToList();
            int userID = Convert.ToInt32(Session["IdSession"]);
            List<Address> userAddress = db.Address.Where(x => x.UserID == userID).ToList();
            IList<SelectListItem> payMeth = new List<SelectListItem>();
            IList<SelectListItem> address = new List<SelectListItem>();

            foreach (var item in paymentsMethods)
            {
                payMeth.Add(new SelectListItem() { Value = item.ID.ToString(), Text = item.Payment1 });
            }

            foreach (var item in userAddress)
            {
                address.Add(new SelectListItem() { Value = item.id.ToString(), Text = item.Address1 });
            }

            order.paymentsMethods = payMeth;
            order.address = address;

            return View(order);
        }

        [HttpPost]
        public ActionResult CreateOrder(OrderEditView order)
        {
            try
            {

                if (order.addressID != null || order.addressID != null || Session["IdSession"] != null)
                {
                    Order selectedOrder = new Order
                    {
                        OrderID = GenerateID(),
                        AddressID = (int)order.addressID,
                        ClientID = Convert.ToInt32(Session["IdSession"]),
                        PaymentID = (int)order.paymentID,
                        StatusID = 5,
                        OrderDate = DateTime.Now
                    };

                    List<ShopingCart> cart = (List<ShopingCart>)Session["cart"];

                    
                    foreach (var obj in cart)
                    {
                        OrderInfo temp = new OrderInfo
                        {
                            OrderID = selectedOrder.OrderID,
                            Price = obj.product.Price,
                            ProductID = obj.productID,
                            Quantity = obj.kiekis,
                            Order = selectedOrder
                        };

                        db3.OrderInfoes.Add(temp);
                    }

                    if(order.paymentID == 1)
                    {
                        selectedOrder.StatusID = 1;
                    }
                    
                    db3.Orders.Add(selectedOrder);
                    Session["cart"] = null;
                    Session["count"] = null;

                    db3.SaveChanges();

                    if(order.paymentID == 3)
                    {
                        return RedirectToAction("PaymentCreditCard", "Order", new { order = selectedOrder.OrderID });
                    }
                    if (order.paymentID == 2)
                    {
                        return RedirectToAction("PaymentBank", "Order", new { order = selectedOrder.OrderID });
                    }


                }
                else
                {
                    return RedirectToAction("Error", "Home");
                }
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult PaymentBank(int? order)
        {
            if (order == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Session["OrderToPay"] = order;
            List<BankViewModel> banks = new List<BankViewModel>();
            BankViewModel bank1 = new BankViewModel { name = "Luminor", immage = "https://kreditai.info/images/kreditoriai/bankai/bankas_luminor.png" };
            banks.Add(bank1);
            return View(banks);
        }

        public ActionResult PaymentBank2(string dummy)
        {
            int orderid = Convert.ToInt32(Session["OrderToPay"]);
            Order order = db3.Orders.Where(x => x.OrderID == orderid).FirstOrDefault();
            order.StatusID = 1;
            db3.SaveChanges();
            Session["OrderToPay"] = null;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult PaymentCreditCard(int? order)
        {
            if(order == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Session["OrderToPay"] = order;
            return View();
        }

        [HttpPost]
        public ActionResult PaymentCreditCard(CreditCard card)
        {
            int orderid = Convert.ToInt32(Session["OrderToPay"]);
            Order order = db3.Orders.Where(x => x.OrderID == orderid).FirstOrDefault();
            order.StatusID = 1;
            db3.SaveChanges();
            Session["OrderToPay"] = null;
            return RedirectToAction("Index", "Home");
        }

        public int GenerateID()
        {
            int generatedID = new Random().Next(999999);
            Order order = db3.Orders.Where(x => x.OrderID == generatedID).FirstOrDefault();
            if (order != null)
            {
                GenerateID();
            }
            else  return generatedID;

            return generatedID;
        }
    }
}