﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITWorld.Models;

namespace ITWorld.ModelView
{
    public class PSU
    {
        public Product product { get; set; }
        public Product_PSU psu { get; set; }
    }
}