﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using System.Web.Mvc.Html;

namespace ITWorld.Controllers
{
    public class HomeController : Controller
    {

        //Duomenu baze
        Entities1 db = new Entities1();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {

            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Register()
        {
            if (Session["IdSession"] != null)
            {
                return RedirectToAction("Error");
            }

            ViewBag.Message = "Vartotojo registracija";

            return View();
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            if(db.User.Any(x=>x.Username == user.Username))
            {
                ViewBag.Notification = "Su tokio slapyvaržiu jau egzistuoja vartotojas";
                return View();
            }
            else
            {
                //Jeigu nera dar pačios pirmos rolės
                if(db.Role.Find(0) == null)
                {
                    Role startRole = new Role();
                    startRole.Role_Name = "Vartotojas";
                    db.Role.Add(startRole);
                    db.SaveChanges();
                }

                db.User.Add(user);
                db.SaveChanges();

                /*
                Session["IdSession"] = user.ID.ToString();
                Session["UserName"] = user.Username.ToString();
                */
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }



        public ActionResult Login(User user)
        {

            var checkLogin = db.User.Where(x => x.Username.Equals(user.Username) && x.Password.Equals(user.Password)).FirstOrDefault();
            if(checkLogin !=null && user.Password == checkLogin.Password)
            {
                Session["IdSession"] = checkLogin.ID.ToString();
                Session["UserName"] = checkLogin.Username.ToString();
                Session["Role"] = GetRole();
                return RedirectToAction("Index", "Home");
            }
            else
            {
                 ViewBag.Notification = "Neteisingi prisijungimo duomenys";
            }
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (Session["IdSession"] != null)
            {
                return RedirectToAction("Error");
            }

            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        public string GetRole()
        {
            return db.User.Find(Convert.ToInt32(Session["IdSession"])).Role.Role_Name;
        }
    }
}