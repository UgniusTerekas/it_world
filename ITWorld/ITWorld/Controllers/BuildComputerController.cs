﻿using ITWorld.Models;
using ITWorld.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITWorld.Controllers
{
    public class BuildComputerController : Controller
    {
        Entities1 db = new Entities1();
        Entities db2 = new Entities();
        OrderModelEntity db3 = new OrderModelEntity();
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult CreatePC()
        {
            ComputerEditModel computerEditModel = new ComputerEditModel();
            PopulateSelections(computerEditModel);
            return View(computerEditModel);
        }

        [HttpPost]
        public ActionResult CreatePC(ComputerEditModel computerEditModel)
        {

            Product product = null;
            if (Valid(computerEditModel))
            {

                product = new Product
                {
                    Name = "Custom Computer",
                    Price = Price(computerEditModel),
                    Manufacturer = "-",
                    Stock = 0,
                    Guarrantee = "2024-01-01",
                    Image_URL = "https://assets.bigcartel.com/product_images/32367145/Custom_Nameweb.jpg?auto=format&fit=max&h=1000&w=1000",
                    Category = "Custom"

                };

                Product_Computer computer = new Product_Computer {

                    Ram_ID = computerEditModel.computer.Ram_ID,
                    MB_ID = computerEditModel.computer.MB_ID,
                    GPU_ID = computerEditModel.computer.GPU_ID,
                    CPU_ID = computerEditModel.computer.CPU_ID,
                    ROM_ID = computerEditModel.computer.ROM_ID,
                    Case_ID = computerEditModel.computer.Case_ID,
                    Cooler_ID = computerEditModel.computer.Cooler_ID,
                    PSU_ID = computerEditModel.computer.PSU_ID
                };

                computer.Product = product;
                product.Product_Computer = computer;
                db2.Product_Computer.Add(computer);
                db2.Product.Add(product);
                db2.SaveChanges();

            }
            else
            {
                PopulateSelections(computerEditModel);
                return View(computerEditModel);
            }

            return RedirectToAction("Add", "Shopping", new { productID = product.ID });
        }

        public bool Valid(ComputerEditModel computerEditModel)
        {
            bool check = true;
            if (computerEditModel.computer.Case_ID == 0 || computerEditModel.computer.Ram_ID == 0 || computerEditModel.computer.CPU_ID == 0 ||
                computerEditModel.computer.MB_ID == 0 || computerEditModel.computer.GPU_ID == 0 || computerEditModel.computer.ROM_ID == 0 ||
                computerEditModel.computer.PSU_ID == 0 || computerEditModel.computer.Cooler_ID == 0)
            {
                check = false;
                ModelState.AddModelError("DetalErr", "Nevisi laukeliai pilnai uzpildyti");
                return check;
            }
            
            Product_CPU CPU = db2.Product_CPU.Where(x => x.ID == computerEditModel.computer.CPU_ID).FirstOrDefault();
            Product_MotherBoard motherboard = db2.Product_MotherBoard.Where(x => x.ID == computerEditModel.computer.MB_ID).FirstOrDefault();
            Product_RAM ram = db2.Product_RAM.Where(x => x.ID == computerEditModel.computer.Ram_ID).FirstOrDefault();


            if (!motherboard.CPU_Socket.Contains(CPU.Generation))
            {
                ModelState.AddModelError("CPUErr", "Procesorius netinka jūsų pasirinktai motininei plokštei");
                check = false;
            }



            if(ram.Type.Contains("DDR3"))
            {
                if(!motherboard.RAM_Compability.Contains("DDR3"))
                {
                    check = false;
                    ModelState.AddModelError("RAMErr", "RAM atmintis netinka jūsų pasirinktai motininei plokštei");
                }
            }

            if (ram.Type.Contains("DDR4"))
            {
                if (!motherboard.RAM_Compability.Contains("DDR4"))
                {
                    check = false;
                    ModelState.AddModelError("RAMErr", "RAM atmintis netinka jūsų pasirinktai motininei plokštei");
                }
            }



            return check;
        }

        public double Price(ComputerEditModel computerEditModel)
        {
            double sum = 0;
            sum += db2.Product_RAM.Where(x => x.ID == computerEditModel.computer.Ram_ID).FirstOrDefault().Product.Price;
            sum += db2.Product_MotherBoard.Where(x => x.ID == computerEditModel.computer.MB_ID).FirstOrDefault().Product.Price;
            sum += db2.Product_Graphics_Card.Where(x => x.ID == computerEditModel.computer.GPU_ID).FirstOrDefault().Product.Price;
            sum += db2.Product_CPU.Where(x => x.ID == computerEditModel.computer.CPU_ID).FirstOrDefault().Product.Price;
            sum += db2.Product_Memory.Where(x => x.ID == computerEditModel.computer.ROM_ID).FirstOrDefault().Product.Price;
            sum += db2.Product_Case.Where(x => x.ID == computerEditModel.computer.Case_ID).FirstOrDefault().Product.Price;
            sum += db2.Product_Cooler.Where(x => x.ID == computerEditModel.computer.Cooler_ID).FirstOrDefault().Product.Price;
            sum += db2.Product_PSU.Where(x => x.ID == computerEditModel.computer.PSU_ID).FirstOrDefault().Product.Price;
            sum += 50;
            return sum;
        }


        public void PopulateSelections(ComputerEditModel computerEditModel)
        {
            var ram = db2.Product_RAM.ToList();
            List<SelectListItem> selectRam = new List<SelectListItem>();

            var cases = db2.Product_Case.ToList();
            List<SelectListItem> selectCase= new List<SelectListItem>();

            var coolers = db2.Product_Cooler.ToList();
            List<SelectListItem> selectcoler = new List<SelectListItem>();

            var cpus = db2.Product_CPU.ToList();
            List<SelectListItem> selectcpu = new List<SelectListItem>();

            var gpus = db2.Product_Graphics_Card.ToList();
            List<SelectListItem> selectgpus = new List<SelectListItem>();

            var memory = db2.Product_Memory.ToList();
            List<SelectListItem> selectmemorys = new List<SelectListItem>();

            var motherboards = db2.Product_MotherBoard.ToList();
            List<SelectListItem> selectmotherboards = new List<SelectListItem>();

            var psus = db2.Product_PSU.ToList();
            List<SelectListItem> selectpsus = new List<SelectListItem>();


            //užpildomas kebulų sąrašas iš duomenų bazės
            foreach (var item in ram)
            {
                selectRam.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Size + " GB," + item.Speed + " Hz," + item.Product.Price + " Eur" });
            }

            foreach (var item in cases)
            {
                selectCase.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Product.Price + " Eur" });
            }

            foreach (var item in coolers)
            {
                selectcoler.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Product.Price + " Eur" });
            }

            foreach (var item in cpus)
            {
                selectcpu.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Speed + " Ghz,"  + item.Product.Price + " Eur" });
            }
            foreach (var item in gpus)
            {
                selectgpus.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Speed + " Ghz," + item.Capacity + " Gb, " + item.Product.Price + " Eur" });
            }
            foreach (var item in memory)
            {
                selectmemorys.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Speed + " Ghz," + item.Capacity + " Gb, " + item.Product.Price + " Eur" });
            }
            foreach (var item in motherboards)
            {
                selectmotherboards.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.CPU_Socket + ", " + item.Product.Price + " Eur" });
            }
            foreach (var item in psus)
            {
                selectpsus.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Power + "W, " + item.Product.Price + " Eur" });
            }





            //Sarašai priskiriami vaizdo objektui
            computerEditModel.Product_RAMs = selectRam;
            computerEditModel.Product_Cases = selectCase;
            computerEditModel.Product_Coolers = selectcoler;
            computerEditModel.Product_CPUs = selectcpu;
            computerEditModel.Product_Graphics_Cards = selectgpus;
            computerEditModel.Product_Memorys = selectmemorys;
            computerEditModel.Product_MotherBoards = selectmotherboards;
            computerEditModel.Product_PSUs = selectpsus;
        }
    }

   
}