﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class ShoppingController : Controller
    {
        Entities db = new Entities();
        // GET: Shopping
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Add(int? productID)
        {
            Product product = db.Product.Where(x => x.ID == productID).FirstOrDefault();


            if (Session["cart"] == null)
            {
                List<ShopingCart> li = new List<ShopingCart>();

                var cartProduct = new ShopingCart { product = product,
                    productID = product.ID,
                    kiekis = 1
                };

                li.Add(cartProduct);
                Session["cart"] = li;
                ViewBag.cart = li.Count();


                Session["count"] = 1;


            }
            else
            {
                List<ShopingCart> li = (List<ShopingCart>)Session["cart"];
                ShopingCart tempObj = li.Where(x => x.productID == productID).FirstOrDefault();
                if(tempObj == null)
                {
                    var cartProduct = new ShopingCart
                    {
                        product = product,
                        productID = product.ID,
                        kiekis = 1
                    };
                    li.Add(cartProduct);
                    Session["count"] = Convert.ToInt32(Session["count"]) + 1;
                }
                else
                {
                    li.Where(x => x.productID == productID).FirstOrDefault().kiekis++;
                }
                Session["cart"] = li;
                ViewBag.cart = li.Count();

            }
            return RedirectToAction("Myorder", "Shopping");


        }

        public ActionResult Myorder()
        {
            return View((List<ShopingCart>)Session["cart"]);
        }

        [HttpGet]
        public ActionResult Remove(int productID)
        {
            List<ShopingCart> li = (List<ShopingCart>)Session["cart"];
            li.RemoveAll(x => x.productID == productID);
            Session["cart"] = li;
            Session["count"] = Convert.ToInt32(Session["count"]) - 1;
            return RedirectToAction("Myorder", "Shopping");

        }
    }
}