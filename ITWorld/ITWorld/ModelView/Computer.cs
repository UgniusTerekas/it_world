﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITWorld.Models;

namespace ITWorld.ModelView
{
    public class Computer
    {
        public Product product { get; set; }
        public Product_Computer computer { get; set; }
    }
}