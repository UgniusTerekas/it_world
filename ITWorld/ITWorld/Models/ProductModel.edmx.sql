
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/07/2021 10:29:21
-- Generated from EDMX file: C:\Users\redas\Desktop\INŽINERIJA\ITWORLD – kopija\ITWorld\ITWorld\Models\ProductModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ITWorld];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Case_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Case] DROP CONSTRAINT [FK_Case_inherits_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_Case]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_Case];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_Cooler]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_Cooler];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_CPU]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_CPU];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_Graphics_Card]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_Graphics_Card];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_inherits_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_Memory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_Memory];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_MotherBoard]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_MotherBoard];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_PSU]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_PSU];
GO
IF OBJECT_ID(N'[dbo].[FK_Computer_RAM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Computer] DROP CONSTRAINT [FK_Computer_RAM];
GO
IF OBJECT_ID(N'[dbo].[FK_Cooler_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Cooler] DROP CONSTRAINT [FK_Cooler_inherits_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_CPU_Cooler]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_CPU] DROP CONSTRAINT [FK_CPU_Cooler];
GO
IF OBJECT_ID(N'[dbo].[FK_CPU_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_CPU] DROP CONSTRAINT [FK_CPU_inherits_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_Graphics_Card_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Graphics_Card] DROP CONSTRAINT [FK_Graphics_Card_inherits_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_Memory_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_Memory] DROP CONSTRAINT [FK_Memory_inherits_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_MotherBoard_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_MotherBoard] DROP CONSTRAINT [FK_MotherBoard_inherits_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_PSU_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_PSU] DROP CONSTRAINT [FK_PSU_inherits_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_RAM_inherits_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Product_RAM] DROP CONSTRAINT [FK_RAM_inherits_Product];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Product]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product];
GO
IF OBJECT_ID(N'[dbo].[Product_Case]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Case];
GO
IF OBJECT_ID(N'[dbo].[Product_Computer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Computer];
GO
IF OBJECT_ID(N'[dbo].[Product_Cooler]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Cooler];
GO
IF OBJECT_ID(N'[dbo].[Product_CPU]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_CPU];
GO
IF OBJECT_ID(N'[dbo].[Product_Graphics_Card]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Graphics_Card];
GO
IF OBJECT_ID(N'[dbo].[Product_Headphones]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Headphones];
GO
IF OBJECT_ID(N'[dbo].[Product_Keyboard]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Keyboard];
GO
IF OBJECT_ID(N'[dbo].[Product_Memory]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Memory];
GO
IF OBJECT_ID(N'[dbo].[Product_Monitor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Monitor];
GO
IF OBJECT_ID(N'[dbo].[Product_MotherBoard]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_MotherBoard];
GO
IF OBJECT_ID(N'[dbo].[Product_Mouse]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Mouse];
GO
IF OBJECT_ID(N'[dbo].[Product_MousePad]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_MousePad];
GO
IF OBJECT_ID(N'[dbo].[Product_PSU]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_PSU];
GO
IF OBJECT_ID(N'[dbo].[Product_RAM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_RAM];
GO
IF OBJECT_ID(N'[dbo].[Product_Strips]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Product_Strips];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Product'
CREATE TABLE [dbo].[Product] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(50)  NOT NULL,
    [Price] float  NOT NULL,
    [Manufacturer] varchar(50)  NOT NULL,
    [Stock] smallint  NOT NULL,
    [Guarrantee] varchar(50)  NULL,
    [Is_RGB] bit  NOT NULL,
    [Image_URL] varchar(max)  NULL,
    [Category] varchar(50)  NOT NULL
);
GO

-- Creating table 'Product_Case'
CREATE TABLE [dbo].[Product_Case] (
    [Side_Window] bit  NOT NULL,
    [Size_cat] varchar(50)  NOT NULL,
    [Dimension] varchar(50)  NOT NULL,
    [Expansio_Slots] int  NOT NULL,
    [Supported_MB] varchar(50)  NOT NULL,
    [Tempered_Glass] bit  NULL,
    [Reset_Button] bit  NULL,
    [C525_slots] int  NOT NULL,
    [C25_slots] int  NOT NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_Computer'
CREATE TABLE [dbo].[Product_Computer] (
    [Ram_ID] int  NOT NULL,
    [MB_ID] int  NOT NULL,
    [GPU_ID] int  NOT NULL,
    [CPU_ID] int  NOT NULL,
    [ROM_ID] int  NOT NULL,
    [Case_ID] int  NOT NULL,
    [Cooler_ID] int  NOT NULL,
    [PSU_ID] int  NOT NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_Cooler'
CREATE TABLE [dbo].[Product_Cooler] (
    [TDP] int  NOT NULL,
    [Size] int  NOT NULL,
    [Water] tinyint  NOT NULL,
    [Color] varchar(50)  NOT NULL,
    [Noise_Level] varchar(50)  NOT NULL,
    [RPM] int  NOT NULL,
    [Compatable_Socket] varchar(max)  NOT NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_CPU'
CREATE TABLE [dbo].[Product_CPU] (
    [CoreCount] varchar(50)  NOT NULL,
    [L1] smallint  NOT NULL,
    [L2] int  NULL,
    [L3] int  NULL,
    [Generation] varchar(50)  NOT NULL,
    [Speed] varchar(50)  NOT NULL,
    [PCIE_Lanes] varchar(50)  NOT NULL,
    [Turbo] tinyint  NOT NULL,
    [Cooler_ID] int  NULL,
    [TDP] int  NOT NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_Graphics_Card'
CREATE TABLE [dbo].[Product_Graphics_Card] (
    [Speed] varchar(50)  NOT NULL,
    [Capacity] int  NOT NULL,
    [Type] varchar(50)  NOT NULL,
    [BitRate] int  NOT NULL,
    [PCIE_Connection] varchar(50)  NOT NULL,
    [Family] varchar(50)  NOT NULL,
    [Dimensions] varchar(50)  NOT NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_HeadphonesSet'
CREATE TABLE [dbo].[Product_Headphones] (
    [Microphone] bit  NOT NULL,
    [Wireless] bit  NOT NULL,
    [Freq_range] varchar(50)  NOT NULL,
    [Wire_length] float  NULL,
    [Distance] float  NULL,
    [Type] varchar(50)  NOT NULL,
    [Connection] varchar(50)  NOT NULL,
    [Total_playtime] varchar(50)  NULL,
    [Bluetooth_version] varchar(50)  NULL,
    [ID] int  NOT NULL,
    [Product_ID] int  NOT NULL
);
GO

-- Creating table 'Product_KeyboardSet'
CREATE TABLE [dbo].[Product_Keyboard] (
    [Wireless] bit  NOT NULL,
    [Layout] varchar(50)  NOT NULL,
    [Size____] tinyint  NOT NULL,
    [Switches] varchar(50)  NULL,
    [Wire_length] float  NULL,
    [Guarantee_presses] int  NULL,
    [ID] int  NOT NULL,
    [Product_ID] int  NOT NULL
);
GO

-- Creating table 'Product_Memory'
CREATE TABLE [dbo].[Product_Memory] (
    [Interface] varchar(50)  NOT NULL,
    [Speed] varchar(50)  NOT NULL,
    [Size] float  NOT NULL,
    [Capacity] int  NOT NULL,
    [Read_Write] int  NOT NULL,
    [RotatioSpeed] int  NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_MonitorSet'
CREATE TABLE [dbo].[Product_Monitor] (
    [Size] float  NOT NULL,
    [Refresh_rate] int  NOT NULL,
    [Input_lag] tinyint  NOT NULL,
    [Surface] varchar(50)  NOT NULL,
    [Panel] varchar(50)  NOT NULL,
    [Color_count] varchar(50)  NOT NULL,
    [Contrast] varchar(50)  NOT NULL,
    [aspect_ratio] varchar(50)  NOT NULL,
    [I_O] varchar(50)  NOT NULL,
    [Brightness] int  NOT NULL,
    [Touch_screen] bit  NULL,
    [ID] int  NOT NULL,
    [Product_ID] int  NOT NULL
);
GO

-- Creating table 'Product_MotherBoard'
CREATE TABLE [dbo].[Product_MotherBoard] (
    [CPU_Socket] varchar(50)  NOT NULL,
    [RAM_Cap] varchar(50)  NOT NULL,
    [RAM_Compability] varchar(50)  NOT NULL,
    [PCIE_Lanes] varchar(max)  NOT NULL,
    [Sound] varchar(50)  NOT NULL,
    [LAN] varchar(50)  NOT NULL,
    [I_O] varchar(max)  NOT NULL,
    [BackPanel] varchar(max)  NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_MouseSet'
CREATE TABLE [dbo].[Product_Mouse] (
    [Wireless] bit  NOT NULL,
    [Wire_length] int  NULL,
    [DPI] int  NOT NULL,
    [Button_count] tinyint  NOT NULL,
    [Type] varchar(50)  NOT NULL,
    [DPI_setting] varchar(50)  NOT NULL,
    [Click_rating] int  NOT NULL,
    [ID] int  NOT NULL,
    [Product_ID] int  NOT NULL
);
GO

-- Creating table 'Product_MousePadSet'
CREATE TABLE [dbo].[Product_MousePad] (
    [ID] int  NOT NULL,
    [Charge_pad] bit  NULL,
    [Size] varchar(50)  NOT NULL,
    [Overview] varchar(max)  NOT NULL,
    [Product_ID] int  NOT NULL
);
GO

-- Creating table 'Product_PSU'
CREATE TABLE [dbo].[Product_PSU] (
    [Eff_Standard] varchar(50)  NOT NULL,
    [Power] smallint  NOT NULL,
    [Modular] tinyint  NOT NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_RAM'
CREATE TABLE [dbo].[Product_RAM] (
    [Size] int  NOT NULL,
    [Speed] int  NOT NULL,
    [Type] varchar(50)  NOT NULL,
    [ID] int  NOT NULL
);
GO

-- Creating table 'Product_StripsSet'
CREATE TABLE [dbo].[Product_Strips] (
    [ID] int  NOT NULL,
    [Length] int  NOT NULL,
    [Power] int  NOT NULL,
    [Mounting] varchar(50)  NOT NULL,
    [Remote] bit  NOT NULL,
    [Modes] varchar(max)  NOT NULL,
    [Product_ID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'Product'
ALTER TABLE [dbo].[Product]
ADD CONSTRAINT [PK_Product]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_Case'
ALTER TABLE [dbo].[Product_Case]
ADD CONSTRAINT [PK_Product_Case]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [PK_Product_Computer]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_Cooler'
ALTER TABLE [dbo].[Product_Cooler]
ADD CONSTRAINT [PK_Product_Cooler]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_CPU'
ALTER TABLE [dbo].[Product_CPU]
ADD CONSTRAINT [PK_Product_CPU]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_Graphics_Card'
ALTER TABLE [dbo].[Product_Graphics_Card]
ADD CONSTRAINT [PK_Product_Graphics_Card]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_HeadphonesSet'
ALTER TABLE [dbo].[Product_Headphones]
ADD CONSTRAINT [PK_Product_Headphones]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_KeyboardSet'
ALTER TABLE [dbo].[Product_Keyboard]
ADD CONSTRAINT [PK_Product_Keyboard]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_Memory'
ALTER TABLE [dbo].[Product_Memory]
ADD CONSTRAINT [PK_Product_Memory]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_MonitorSet'
ALTER TABLE [dbo].[Product_Monitor]
ADD CONSTRAINT [PK_Product_Monitor]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_MotherBoard'
ALTER TABLE [dbo].[Product_MotherBoard]
ADD CONSTRAINT [PK_Product_MotherBoard]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_MouseSet'
ALTER TABLE [dbo].[Product_Mouse]
ADD CONSTRAINT [PK_Product_Mouse]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_MousePadSet'
ALTER TABLE [dbo].[Product_MousePad]
ADD CONSTRAINT [PK_Product_MousePad]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_PSU'
ALTER TABLE [dbo].[Product_PSU]
ADD CONSTRAINT [PK_Product_PSU]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_RAM'
ALTER TABLE [dbo].[Product_RAM]
ADD CONSTRAINT [PK_Product_RAM]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Product_StripsSet'
ALTER TABLE [dbo].[Product_Strips]
ADD CONSTRAINT [PK_Product_Strips]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ID] in table 'Product_Case'
ALTER TABLE [dbo].[Product_Case]
ADD CONSTRAINT [FK_Case_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ID] in table 'Product_Cooler'
ALTER TABLE [dbo].[Product_Cooler]
ADD CONSTRAINT [FK_Cooler_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ID] in table 'Product_CPU'
ALTER TABLE [dbo].[Product_CPU]
ADD CONSTRAINT [FK_CPU_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ID] in table 'Product_Graphics_Card'
ALTER TABLE [dbo].[Product_Graphics_Card]
ADD CONSTRAINT [FK_Graphics_Card_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ID] in table 'Product_Memory'
ALTER TABLE [dbo].[Product_Memory]
ADD CONSTRAINT [FK_Memory_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ID] in table 'Product_MotherBoard'
ALTER TABLE [dbo].[Product_MotherBoard]
ADD CONSTRAINT [FK_MotherBoard_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ID] in table 'Product_PSU'
ALTER TABLE [dbo].[Product_PSU]
ADD CONSTRAINT [FK_PSU_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ID] in table 'Product_RAM'
ALTER TABLE [dbo].[Product_RAM]
ADD CONSTRAINT [FK_RAM_inherits_Product]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Case_ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_Case]
    FOREIGN KEY ([Case_ID])
    REFERENCES [dbo].[Product_Case]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Computer_Case'
CREATE INDEX [IX_FK_Computer_Case]
ON [dbo].[Product_Computer]
    ([Case_ID]);
GO

-- Creating foreign key on [Cooler_ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_Cooler]
    FOREIGN KEY ([Cooler_ID])
    REFERENCES [dbo].[Product_Cooler]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Computer_Cooler'
CREATE INDEX [IX_FK_Computer_Cooler]
ON [dbo].[Product_Computer]
    ([Cooler_ID]);
GO

-- Creating foreign key on [CPU_ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_CPU]
    FOREIGN KEY ([CPU_ID])
    REFERENCES [dbo].[Product_CPU]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Computer_CPU'
CREATE INDEX [IX_FK_Computer_CPU]
ON [dbo].[Product_Computer]
    ([CPU_ID]);
GO

-- Creating foreign key on [GPU_ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_Graphics_Card]
    FOREIGN KEY ([GPU_ID])
    REFERENCES [dbo].[Product_Graphics_Card]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Computer_Graphics_Card'
CREATE INDEX [IX_FK_Computer_Graphics_Card]
ON [dbo].[Product_Computer]
    ([GPU_ID]);
GO

-- Creating foreign key on [ROM_ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_Memory]
    FOREIGN KEY ([ROM_ID])
    REFERENCES [dbo].[Product_Memory]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Computer_Memory'
CREATE INDEX [IX_FK_Computer_Memory]
ON [dbo].[Product_Computer]
    ([ROM_ID]);
GO

-- Creating foreign key on [MB_ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_MotherBoard]
    FOREIGN KEY ([MB_ID])
    REFERENCES [dbo].[Product_MotherBoard]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Computer_MotherBoard'
CREATE INDEX [IX_FK_Computer_MotherBoard]
ON [dbo].[Product_Computer]
    ([MB_ID]);
GO

-- Creating foreign key on [PSU_ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_PSU]
    FOREIGN KEY ([PSU_ID])
    REFERENCES [dbo].[Product_PSU]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Computer_PSU'
CREATE INDEX [IX_FK_Computer_PSU]
ON [dbo].[Product_Computer]
    ([PSU_ID]);
GO

-- Creating foreign key on [Ram_ID] in table 'Product_Computer'
ALTER TABLE [dbo].[Product_Computer]
ADD CONSTRAINT [FK_Computer_RAM]
    FOREIGN KEY ([Ram_ID])
    REFERENCES [dbo].[Product_RAM]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Computer_RAM'
CREATE INDEX [IX_FK_Computer_RAM]
ON [dbo].[Product_Computer]
    ([Ram_ID]);
GO

-- Creating foreign key on [Cooler_ID] in table 'Product_CPU'
ALTER TABLE [dbo].[Product_CPU]
ADD CONSTRAINT [FK_CPU_Cooler]
    FOREIGN KEY ([Cooler_ID])
    REFERENCES [dbo].[Product_Cooler]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CPU_Cooler'
CREATE INDEX [IX_FK_CPU_Cooler]
ON [dbo].[Product_CPU]
    ([Cooler_ID]);
GO

-- Creating foreign key on [Product_ID] in table 'Product_HeadphonesSet'
ALTER TABLE [dbo].[Product_Headphones]
ADD CONSTRAINT [FK_ProductProduct_Headphones]
    FOREIGN KEY ([Product_ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductProduct_Headphones'
CREATE INDEX [IX_FK_ProductProduct_Headphones]
ON [dbo].[Product_Headphones]
    ([Product_ID]);
GO

-- Creating foreign key on [Product_ID] in table 'Product_KeyboardSet'
ALTER TABLE [dbo].[Product_Keyboard]
ADD CONSTRAINT [FK_ProductProduct_Keyboard]
    FOREIGN KEY ([Product_ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductProduct_Keyboard'
CREATE INDEX [IX_FK_ProductProduct_Keyboard]
ON [dbo].[Product_Keyboard]
    ([Product_ID]);
GO

-- Creating foreign key on [Product_ID] in table 'Product_MonitorSet'
ALTER TABLE [dbo].[Product_Monitor]
ADD CONSTRAINT [FK_ProductProduct_Monitor]
    FOREIGN KEY ([Product_ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductProduct_Monitor'
CREATE INDEX [IX_FK_ProductProduct_Monitor]
ON [dbo].[Product_Monitor]
    ([Product_ID]);
GO

-- Creating foreign key on [Product_ID] in table 'Product_MouseSet'
ALTER TABLE [dbo].[Product_Mouse]
ADD CONSTRAINT [FK_ProductProduct_Mouse]
    FOREIGN KEY ([Product_ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductProduct_Mouse'
CREATE INDEX [IX_FK_ProductProduct_Mouse]
ON [dbo].[Product_Mouse]
    ([Product_ID]);
GO

-- Creating foreign key on [Product_ID] in table 'Product_MousePadSet'
ALTER TABLE [dbo].[Product_MousePad]
ADD CONSTRAINT [FK_ProductProduct_MousePad]
    FOREIGN KEY ([Product_ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductProduct_MousePad'
CREATE INDEX [IX_FK_ProductProduct_MousePad]
ON [dbo].[Product_MousePad]
    ([Product_ID]);
GO

-- Creating foreign key on [Product_ID] in table 'Product_StripsSet'
ALTER TABLE [dbo].[Product_Strips]
ADD CONSTRAINT [FK_ProductProduct_Strips]
    FOREIGN KEY ([Product_ID])
    REFERENCES [dbo].[Product]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductProduct_Strips'
CREATE INDEX [IX_FK_ProductProduct_Strips]
ON [dbo].[Product_Strips]
    ([Product_ID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------