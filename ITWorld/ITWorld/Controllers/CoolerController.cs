﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class CoolerController : Controller
    {
        // GET: Cooler
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_Cooler> coolers = new List<Product_Cooler>();
            coolers = db.Product_Cooler.ToList();
            int min = (int)coolers.Min(c => c.Product.Price);
            int max = (int)coolers.Max(c => c.Product.Price);
            ViewBag.min = min;
            ViewBag.max = max;
            return View(coolers);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Cooler> coolers = new List<Product_Cooler>();
            coolers = db.Product_Cooler.ToList();
            if (coolers.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)coolers.Min(c => c.Product.Price);
                int masterMax = (int)coolers.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(coolers);
            }

            return View(coolers.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_Cooler.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}