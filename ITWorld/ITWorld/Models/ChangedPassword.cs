﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ITWorld.Models
{
    public partial class ChangedPassword
    {
        [Display(Name = "Senas slaptažodis")]
        [DataType(DataType.Password)]
        public string oldPassword {get;set;}

        [Display(Name = "Naujas slaptažodis")]
        [DataType(DataType.Password)]
        public string newPassword { get; set; }

        [Display(Name = "Pakartoti slaptažodį")]
        [DataType(DataType.Password)]
        public string reEnterPassword { get; set; }
    }
}
