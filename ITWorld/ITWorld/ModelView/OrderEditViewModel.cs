﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITWorld.ModelView
{
    public class OrderEditViewModel
    {
        public int OrderID { get; set; }
        [DisplayName("Užsakymo data")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime OrderDate { get; set; }
        [DisplayName("Apmokėjimo būdas")]
        [Required]
        public int PaymentID { get; set; }
        [DisplayName("Užsakymo būsena")]
        [Required]
        public int StatusID { get; set; }

        [DisplayName("Vartotojo pristatymo adresai")]
        [Required]
        public int AddressID { get; set; }
        public int ClientID { get; set; }
        public IList<SelectListItem> payments { get; set; }
        public IList<SelectListItem> status { get; set; }
        public IList<SelectListItem> Address { get; set; }
    }
}