﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ITWorld.ModelView
{
    public class CreditCard
    {
        [Display(Name = "Kreditinės kortelės numeris")]
        [Required(ErrorMessage = "Privaloma")]
        [Range(100000000000, 9999999999999999999, ErrorMessage = "Neteisingai nurodytas kortelės numeris")]
        public long CardNumber { get; set; }

        [Display(Name = "Kreditinės kortelės galiojimas")]
        [Required(ErrorMessage = "Privaloma")]
        public string Valid { get; set; }

        [Display(Name = "Savininkas")]
        [Required(ErrorMessage = "Privaloma")]
        public string Owner { get; set; }

        [Display(Name = "CVV")]
        [Required(ErrorMessage = "Privaloma")]
        public string CVV { get; set; }
    }
}