﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class AdminController : Controller
    {

        Entities1 db = new Entities1();
        Entities db2 = new Entities();
        OrderModelEntity db3 = new OrderModelEntity();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AdminMeniu()
        {
            if (Session["IdSession"] == null || Convert.ToString(Session["Role"]) != "Administratorius")
            {
                return RedirectToAction("Error", "Home");
            }

            return View();
        }

        [HttpGet]
        public ActionResult ViewUsers()
        {
            if (Session["IdSession"] == null || Convert.ToString(Session["Role"]) != "Administratorius")
            {
                return RedirectToAction("Error", "Home");
            }

            return View(db.User.ToList());
        }


        [HttpGet]
        public ActionResult ViewInfo(int? id)
        {
            if (Session["IdSession"] == null || Convert.ToString(Session["Role"]) != "Administratorius")
            {
                return RedirectToAction("Error", "Home");
            }

            if (id != null)
            {
                var user = db.User.Find(id);
                var address = user.Address.ToList();
                var t = new Tuple<User, List<Address>>(user, address);
                return View(t);

            }
            else return RedirectToAction("ViewUsers");
        }


        public ActionResult DeleteUser(int? id)
        {
            if (Session["IdSession"] == null || Convert.ToString(Session["Role"]) != "Administratorius")
            {
                return RedirectToAction("Error", "Home");
            }

            return View(db.User.Where(x=>x.ID == id).FirstOrDefault());
        }


        [HttpPost]
        public ActionResult DeleteUser(int? id, FormCollection collection)
        {
            if (Session["IdSession"] == null || Convert.ToString(Session["Role"]) != "Administratorius")
            {
                return RedirectToAction("Error", "Home");
            }

            if (id != null)
            {
                User usr = db.User.Where(x => x.ID == id).FirstOrDefault();
                db.User.Remove(usr);
                db.SaveChanges();
                return RedirectToAction("ViewUsers");

            }
            else return RedirectToAction("ViewUsers");
        }


        public ActionResult EditUser(int id)
        {
            if (Session["IdSession"] == null || Convert.ToString(Session["Role"]) != "Administratorius")
            {
                return RedirectToAction("Error", "Home");
            }

            return View(db.User.Where(x => x.ID == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult EditUser(int id, User user)
        {
            try
            {
                db.User.Find(id).Username = user.Username;
                db.User.Find(id).Name = user.Name;
                db.User.Find(id).Last_Name = user.Last_Name;
                db.User.Find(id).Password = user.Password;
                db.User.Find(id).Email = user.Email;
                db.User.Find(id).RoleID = user.RoleID;
                db.SaveChanges();
                return RedirectToAction("ViewUsers");
            }
            catch
            {
                return RedirectToAction("ViewUsers");
            }
        }

        public ActionResult UserOrders()
        {
            if (Session["IdSession"] == null || Convert.ToString(Session["Role"]) != "Administratorius")
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {

                List<Order> orders = db3.Orders.ToList();

                List<OrderViewList> ordersView = new List<OrderViewList>();


                foreach (var order in orders)
                {
                    ordersView.Add(
                        new OrderViewList
                        {
                            OrderID = order.OrderID.ToString(),
                            PaymentWay = db3.Payments.Where(x => x.ID == order.PaymentID).FirstOrDefault().Payment1,
                            orderDate = order.OrderDate,
                            Status = db3.Status.Where(x => x.ID == order.StatusID).FirstOrDefault().Status1,
                            Sum = order.OrderInfoes.Sum(x => x.Price)
                        });
                }

                return View(ordersView);
            }
        }

        public ActionResult EditUserOrder(string id)
        {
            int orderID = Convert.ToInt32(id);
            Order tempOrder = db3.Orders.Where(x => x.OrderID == orderID).FirstOrDefault();
            OrderEditViewModel temp = new OrderEditViewModel();
            temp.OrderID = tempOrder.OrderID;
            temp.PaymentID = tempOrder.PaymentID;
            temp.OrderDate = tempOrder.OrderDate;
            temp.StatusID = tempOrder.StatusID;
            temp.AddressID = tempOrder.AddressID;
            temp.ClientID = tempOrder.ClientID;
            PopulateSelections(temp);
            return View(temp);
        }

        [HttpPost]
        public ActionResult EditUserOrder(string id, OrderEditViewModel collection)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    int orderID = Convert.ToInt32(id);
                    Order temp = db3.Orders.Where(x => x.OrderID == orderID).FirstOrDefault();
                    temp.PaymentID = collection.PaymentID;
                    temp.OrderDate = collection.OrderDate;
                    temp.StatusID = collection.StatusID;
                    temp.AddressID = collection.AddressID;
                    db3.SaveChanges();
                }
                return RedirectToAction("UserOrders");
            }
            catch(Exception e)
            {
                PopulateSelections(collection);
                return View(collection);
            }

        }

        public void PopulateSelections(OrderEditViewModel order)
        {
            var addresai = db.Address.Where(x=>x.UserID == order.ClientID).ToList();
            var mokejimai = db3.Payments.ToList();
            var statutai = db3.Status.ToList();
            IList<SelectListItem> addresaiList = new List<SelectListItem>();
            IList<SelectListItem> mokejimaiList = new List<SelectListItem>();
            IList<SelectListItem> statutaiList = new List<SelectListItem>();

            foreach (var item in addresai)
            {
                addresaiList.Add(new SelectListItem() { Value = Convert.ToString(item.id), Text = item.Address1});
            }


            foreach (var item in mokejimai)
            {
                mokejimaiList.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Payment1 });
            }


            foreach (var item in statutai)
            {
                statutaiList.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Status1 });
            }

            order.Address = addresaiList;
            order.payments = mokejimaiList;
            order.status = statutaiList;
        }
    }
}