﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITWorld.Models;

namespace ITWorld.ModelView
{
    public class GraphicsCard
    {
        public Product product { get; set; }
        public Product_Graphics_Card gpu { get; set; }
    }
}