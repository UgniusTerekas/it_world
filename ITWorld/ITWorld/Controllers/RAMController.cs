﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class RAMController : Controller
    {
        // GET: RAM
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_RAM> ram = new List<Product_RAM>();
            ram = db.Product_RAM.ToList();
            if(ram.Count > 0)
            {
                int min = (int)ram.Min(c => c.Product.Price);
                int max = (int)ram.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }
            return View(ram);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_RAM> ram = new List<Product_RAM>();
            ram = db.Product_RAM.ToList();
            if (ram.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)ram.Min(c => c.Product.Price);
                int masterMax = (int)ram.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(ram);
            }


            return View(ram.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_RAM.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}