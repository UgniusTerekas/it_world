﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class MousepadController : Controller
    {
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_MousePad> mousepads = new List<Product_MousePad>();
            mousepads = db.Product_MousePad.ToList();
            if (mousepads.Count > 0)
            {
                int min = (int)mousepads.Min(c => c.Product.Price);
                int max = (int)mousepads.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(mousepads);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_MousePad> mousepads = new List<Product_MousePad>();
            mousepads = db.Product_MousePad.ToList();
            if (mousepads.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)mousepads.Min(c => c.Product.Price);
                int masterMax = (int)mousepads.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(mousepads);
            }

            return View(mousepads.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_MousePad.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}