﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;


namespace ITWorld.Controllers
{
    public class UserController : Controller
    {
        Entities1 db = new Entities1();
        Entities db2 = new Entities();
        OrderModelEntity db3 = new OrderModelEntity();

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserInformation()
        {
            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }

            User user = db.User.Find(Convert.ToInt32(Session["IdSession"]));
            ViewBag.Username = user.Username.ToString();
            ViewBag.Name = user.Name.ToString();
            ViewBag.Lastname = user.Last_Name.ToString();
            ViewBag.Email = user.Email.ToString();
            ViewBag.Role = user.Role.Role_Name;
            return View();
        }

        public ActionResult ChangeData()
        {
            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }

            return View();
        }


        public ActionResult ChangeEmail()
        {
            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult ChangeEmail(User user)
        {
            db.User.Find(Convert.ToInt32(Session["IdSession"])).Email = user.Email;
            db.SaveChanges();
            Response.Write("<script>alert('El. paštas pakeistas');</script>");

            return View();
        }


        public ActionResult ChangePassword()
        {
            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangedPassword passwords)
        {
            string userPass = db.User.Find(Convert.ToInt32(Session["IdSession"])).Password;
            if (userPass == passwords.oldPassword)
            {

                if (passwords.newPassword == passwords.reEnterPassword)
                {
                    db.User.Find(Convert.ToInt32(Session["IdSession"])).Password = passwords.newPassword;
                    db.SaveChanges();
                    ViewBag.Notification = "Slaptažodis sėkmingai pakeistas";
                }
                else
                {
                    ViewBag.Notification = "Naujas slaptažodžiai nesutampa, prašome patikrinti dar kartą";
                    return View();
                }
            }
            else
            {
                ViewBag.Notification = "Senas slaptažodis neteisingas";
                return View();
            }


            return View();
        }

        [HttpGet]
        public ActionResult DeliveryAddress()
        {
            var v = db.User.Find(Convert.ToInt32(Session["IdSession"])).Address.ToList();
            return View(db.User.Find(Convert.ToInt32(Session["IdSession"])).Address.ToList());
        }



        public ActionResult DeleteAddress(int? id)
        {
            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }

            var ad = db.Address.Where(x => x.id == id).FirstOrDefault();
            db.Address.Remove(ad);
            db.SaveChanges();
            return RedirectToAction("DeliveryAddress", "User");
        }

        public ActionResult AddAddress()
        {
            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddAddress(Address address)
        {
            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }

            int id = Convert.ToInt32(Session["IdSession"]);
            address.User = db.User.Where(x => x.ID.Equals(id)).FirstOrDefault();
            address.UserID = Convert.ToInt32(Session["IdSession"]);

            db.Address.Add(address);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {

            }

            if (db.GetValidationErrors().Any() == false)
                Response.Write("<script type='text/javascript' language='javascript'> alert('Adresas sėkmingai pridėtas')</script>");
            return View();
        }

        public ActionResult UserOrders()
        {
            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                int userID = Convert.ToInt32(Session["IdSession"]);

                List<Order> orders = db3.Orders.Where(x => x.ClientID == userID).ToList();

                List<OrderViewList> ordersView = new List<OrderViewList>();


                foreach (var order in orders)
                {
                    ordersView.Add(
                        new OrderViewList
                        {
                            OrderID = order.OrderID.ToString(),
                            PaymentWay = db3.Payments.Where(x => x.ID == order.PaymentID).FirstOrDefault().Payment1,
                            orderDate = order.OrderDate,
                            Status = db3.Status.Where(x => x.ID == order.StatusID).FirstOrDefault().Status1,
                            Sum = order.OrderInfoes.Sum(x => x.Price)
                        });
                }

                return View(ordersView);
            }
        }

        public ActionResult OrderInformation(string id)
        {
            int userID;

            if (Session["IdSession"] == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                userID = Convert.ToInt32(Session["IdSession"]);
            }

            int orderID = Convert.ToInt32(id);
            Order order = db3.Orders.Where(x => x.OrderID == orderID).FirstOrDefault();

            if (userID != order.ClientID)
                return RedirectToAction("Error", "Home");

            OrderViewInformation orderInfo = new OrderViewInformation
            {
                OrderID = order.OrderID.ToString(),
                PaymentWay = db3.Payments.Where(x => x.ID == order.PaymentID).FirstOrDefault().Payment1,
                orderDate = order.OrderDate,
                Status = db3.Status.Where(x => x.ID == order.StatusID).FirstOrDefault().Status1,
                Sum = order.OrderInfoes.Sum(x => x.Price),
                cart = filledCart(order)
            };

            return View(orderInfo);
        }


        public List<ShopingCart> filledCart(Order order)
        {
            List<ShopingCart> cart = new List<ShopingCart>();

            foreach(var product in order.OrderInfoes)
            {
                cart.Add(new ShopingCart
                {
                    productID = product.ID,
                    kiekis = product.Quantity,
                    product = db2.Product.Where(x=>x.ID == product.ProductID).FirstOrDefault()
                });
            }

            return cart;
        }
    }
}