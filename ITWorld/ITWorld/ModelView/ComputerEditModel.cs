﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;

namespace ITWorld.ModelView
{
    public class ComputerEditModel
    {
        public Product Product { get; set; }
        public Product_Computer computer { get; set; }

        public IList<SelectListItem> Product_Cases { get; set; }
        public IList<SelectListItem> Product_Coolers { get; set; }
        public IList<SelectListItem> Product_CPUs { get; set; }
        public IList<SelectListItem> Product_Graphics_Cards{ get; set; }
        public IList<SelectListItem> Product_Memorys{ get; set; }
        public IList<SelectListItem> Product_MotherBoards { get; set; }
        public IList<SelectListItem> Product_PSUs { get; set; }
        public IList<SelectListItem> Product_RAMs { get; set; }
    }
}