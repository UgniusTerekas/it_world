﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class ComputerController : Controller
    {
        // GET: Computer
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_Computer> computers = new List<Product_Computer>();
             computers = db.Product_Computer.ToList();
            int min = (int)computers.Min(c => c.Product.Price);
            int max = (int)computers.Max(c => c.Product.Price);
            ViewBag.min = min;
            ViewBag.max = max;
            return View(computers);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Computer> computers = new List<Product_Computer>();
            computers = db.Product_Computer.ToList();
            if (computers.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)computers.Min(c => c.Product.Price);
                int masterMax = (int)computers.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(computers);
            }

            return View(computers.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_Computer.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}