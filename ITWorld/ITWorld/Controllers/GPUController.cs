﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class GPUController : Controller
    {
        // GET: GPU
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_Graphics_Card> gpus = new List<Product_Graphics_Card>();
            gpus = db.Product_Graphics_Card.ToList();
            if (gpus.Count > 0)
            {
                int min = (int)gpus.Min(c => c.Product.Price);
                int max = (int)gpus.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(gpus);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Graphics_Card> gpus = new List<Product_Graphics_Card>();
            gpus = db.Product_Graphics_Card.ToList();
            if (gpus.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)gpus.Min(c => c.Product.Price);
                int masterMax = (int)gpus.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(gpus);
            }

            return View(gpus.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_Graphics_Card.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}