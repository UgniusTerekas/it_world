﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class MemoryController : Controller
    {
        // GET: Memory
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_Memory> memories = new List<Product_Memory>();
            memories = db.Product_Memory.ToList();
            if (memories.Count > 0)
            {
                int min = (int)memories.Min(c => c.Product.Price);
                int max = (int)memories.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(memories);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Memory> memories = new List<Product_Memory>();
            memories = db.Product_Memory.ToList();
            if (memories.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)memories.Min(c => c.Product.Price);
                int masterMax = (int)memories.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(memories);
            }

            return View(memories.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_Memory.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}