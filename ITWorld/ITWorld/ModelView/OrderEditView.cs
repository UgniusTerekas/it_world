﻿using ITWorld.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITWorld.ModelView
{
    public class OrderEditView
    {
        [Required(ErrorMessage = "Šis laukas yra privalomas")]
        [Display(Name = "Pristatymo adresas")]
        public int? addressID { get; set; }
        [Required(ErrorMessage = "Šis laukas yra privalomas")]
        [Display(Name = "Apmokėjimo būdas")]
        public int? paymentID { get; set; }
        public IList<SelectListItem> paymentsMethods { get; set; }

        public IList<SelectListItem> address { get; set; }
    }
}