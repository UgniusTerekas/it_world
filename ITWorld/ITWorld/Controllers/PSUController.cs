﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class PSUController : Controller
    {
        // GET: PSU
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_PSU> psus = new List<Product_PSU>();
            psus = db.Product_PSU.ToList();
            if (psus.Count > 0)
            {
                int min = (int)psus.Min(c => c.Product.Price);
                int max = (int)psus.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(psus);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_PSU> psus = new List<Product_PSU>();
            psus = db.Product_PSU.ToList();
            if(psus.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)psus.Min(c => c.Product.Price);
                int masterMax = (int)psus.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(psus);
            }

            return View(psus.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_PSU.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}