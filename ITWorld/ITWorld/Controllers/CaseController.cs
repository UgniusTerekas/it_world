﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class CaseController : Controller
    {
        // GET: Case
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_Case> cases = new List<Product_Case>();
            cases = db.Product_Case.ToList();
            if (cases.Count > 0)
            {
                int min = (int)cases.Min(c => c.Product.Price);
                int max = (int)cases.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(cases);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Case> cases = new List<Product_Case>();
            cases = db.Product_Case.ToList();
            if (cases.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)cases.Min(c => c.Product.Price);
                int masterMax = (int)cases.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(cases);
            }

            return View(cases.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_Case.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}