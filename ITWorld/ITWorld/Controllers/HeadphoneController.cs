﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class HeadphoneController : Controller
    {
        // GET: Case
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_Headphones> headphones = new List<Product_Headphones>();
            headphones = db.Product_Headphones.ToList();
            if (headphones.Count > 0)
            {
                int min = (int)headphones.Min(c => c.Product.Price);
                int max = (int)headphones.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
            }

            return View(headphones);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Headphones> headphones = new List<Product_Headphones>();
            headphones = db.Product_Headphones.ToList();
            if (headphones.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)headphones.Min(c => c.Product.Price);
                int masterMax = (int)headphones.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(headphones);
            }

            return View(headphones.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_Headphones.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}