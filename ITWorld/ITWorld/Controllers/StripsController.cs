﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class StripsController : Controller
    {
        // GET: Strips
        Entities db = new Entities();
        public ActionResult Index()
        {
            List<Product_Strips> strips = new List<Product_Strips>();
            strips = db.Product_Strips.ToList();
            if (strips.Count > 0)
            {
                int min = (int)strips.Min(c => c.Product.Price);
                int max = (int)strips.Max(c => c.Product.Price);
                ViewBag.min = min;
                ViewBag.max = max;
            }
            else ViewBag.Empty = "Tuščias sąrašas";

            return View(strips);
        }
        [HttpPost]
        public ActionResult Index(int min, int max)
        {
            List<Product_Strips> strips = new List<Product_Strips>();
            strips = db.Product_Strips.ToList();
            if(strips.Count > 0)
            {
                min = Convert.ToInt32(Request["min"].ToString());
                max = Convert.ToInt32(Request["max"].ToString());
                int masterMin = (int)strips.Min(c => c.Product.Price);
                int masterMax = (int)strips.Max(c => c.Product.Price);

                ViewBag.masterMin = masterMin;
                ViewBag.masterMax = masterMax;

                ViewBag.min = min;
                ViewBag.max = max;
            }
            else
            {
                ViewBag.Empty = "Tuščias sąrašas";
                return View(strips);
            }
            return View(strips.Where(x => x.Product.Price >= min && x.Product.Price <= max).ToList());
        }


        public ActionResult ViewInfo(int? id)
        {
            return View(db.Product_Strips.Where(x => x.ID == id).FirstOrDefault());
        }
    }
}