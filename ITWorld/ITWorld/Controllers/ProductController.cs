﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITWorld.Models;
using ITWorld.ModelView;

namespace ITWorld.Controllers
{
    public class ProductController : Controller
    {
        Entities db = new Entities();
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddCase()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCase(Case cas)
        {
            db.Product.Add(cas.product);
            db.Product_Case.Add(cas.pcase);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddComputer()
        {
            ComputerEditModel computerEditModel = new ComputerEditModel();
            PopulateSelections(computerEditModel);
            return View(computerEditModel);
        }
        [HttpPost]
        public ActionResult AddComputer(ComputerEditModel comp)
        {
            if (Valid(comp))
            {
                db.Product.Add(comp.Product);
                db.Product_Computer.Add(comp.computer);
                db.SaveChanges();
                return View();
            }
            else
            {
                PopulateSelections(comp);
                return View(comp);
            }
        }
        public ActionResult AddCooler()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCooler(Cooler cool)
        {
            db.Product.Add(cool.product);
            db.Product_Cooler.Add(cool.cooler);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddCPU()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCPU(CPU cp)
        {
            db.Product.Add(cp.product);
            db.Product_CPU.Add(cp.cpu);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddGraphicsCard()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddGraphicsCard(GraphicsCard gp)
        {
            db.Product.Add(gp.product);
            db.Product_Graphics_Card.Add(gp.gpu);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddHeadphones()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddHeadphones(Headphones head)
        {
            db.Product.Add(head.product);
            db.Product_Headphones.Add(head.headphones);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddKeyboard()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddKeyboard(Keyboard key)
        {
            db.Product.Add(key.product);
            db.Product_Keyboard.Add(key.keyboard);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddMemory()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddMemory(Memory mem)
        {
            db.Product.Add(mem.product);
            db.Product_Memory.Add(mem.memory);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddMonitor()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddMonitor(Monitor monito)
        {
            db.Product.Add(monito.product);
            db.Product_Monitor.Add(monito.monitor);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddMB()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddMB(MotherBoard mb)
        {
            db.Product.Add(mb.product);
            db.Product_MotherBoard.Add(mb.motherBoard);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddPSU()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddPSU(PSU ps)
        {
            db.Product.Add(ps.product);
            db.Product_PSU.Add(ps.psu);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddRAM()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddRAM(RAM ra)
        {
            db.Product.Add(ra.product);
            db.Product_RAM.Add(ra.ram);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddStrips()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddStrips(Strips strip)
        {
            db.Product.Add(strip.product);
            db.Product_Strips.Add(strip.strips);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddMouse()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddMouse(Mouse mous)
        {
            db.Product.Add(mous.product);
            db.Product_Mouse.Add(mous.mouse);
            db.SaveChanges();
            return View();
        }
        public ActionResult AddMousePad()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddMousePad(MousePad pad)
        {
            db.Product.Add(pad.product);
            db.Product_MousePad.Add(pad.mousePad);
            db.SaveChanges();
            return View();
        }
        public void PopulateSelections(ComputerEditModel computerEditModel)
        {
            var ram = db.Product_RAM.ToList();
            List<SelectListItem> selectRam = new List<SelectListItem>();

            var cases = db.Product_Case.ToList();
            List<SelectListItem> selectCase = new List<SelectListItem>();

            var coolers = db.Product_Cooler.ToList();
            List<SelectListItem> selectcoler = new List<SelectListItem>();

            var cpus = db.Product_CPU.ToList();
            List<SelectListItem> selectcpu = new List<SelectListItem>();

            var gpus = db.Product_Graphics_Card.ToList();
            List<SelectListItem> selectgpus = new List<SelectListItem>();

            var memory = db.Product_Memory.ToList();
            List<SelectListItem> selectmemorys = new List<SelectListItem>();

            var motherboards = db.Product_MotherBoard.ToList();
            List<SelectListItem> selectmotherboards = new List<SelectListItem>();

            var psus = db.Product_PSU.ToList();
            List<SelectListItem> selectpsus = new List<SelectListItem>();


            //užpildomas kebulų sąrašas iš duomenų bazės
            foreach (var item in ram)
            {
                selectRam.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Size + " GB," + item.Speed + " Hz," + item.Product.Price + " Eur" });
            }

            foreach (var item in cases)
            {
                selectCase.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Product.Price + " Eur" });
            }

            foreach (var item in coolers)
            {
                selectcoler.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Product.Price + " Eur" });
            }

            foreach (var item in cpus)
            {
                selectcpu.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Speed + " Ghz," + item.Product.Price + " Eur" });
            }
            foreach (var item in gpus)
            {
                selectgpus.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Speed + " Ghz," + item.Capacity + " Gb, " + item.Product.Price + " Eur" });
            }
            foreach (var item in memory)
            {
                selectmemorys.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Speed + " Ghz," + item.Capacity + " Gb, " + item.Product.Price + " Eur" });
            }
            foreach (var item in motherboards)
            {
                selectmotherboards.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.CPU_Socket + ", " + item.Product.Price + " Eur" });
            }
            foreach (var item in psus)
            {
                selectpsus.Add(new SelectListItem() { Value = Convert.ToString(item.ID), Text = item.Product.Name + ", " + item.Power + "W, " + item.Product.Price + " Eur" });
            }





            //Sarašai priskiriami vaizdo objektui
            computerEditModel.Product_RAMs = selectRam;
            computerEditModel.Product_Cases = selectCase;
            computerEditModel.Product_Coolers = selectcoler;
            computerEditModel.Product_CPUs = selectcpu;
            computerEditModel.Product_Graphics_Cards = selectgpus;
            computerEditModel.Product_Memorys = selectmemorys;
            computerEditModel.Product_MotherBoards = selectmotherboards;
            computerEditModel.Product_PSUs = selectpsus;
        }
        public bool Valid(ComputerEditModel computerEditModel)
        {
            bool check = true;
            if (computerEditModel.computer.Case_ID == 0 || computerEditModel.computer.Ram_ID == 0 || computerEditModel.computer.CPU_ID == 0 ||
                computerEditModel.computer.MB_ID == 0 || computerEditModel.computer.GPU_ID == 0 || computerEditModel.computer.ROM_ID == 0 ||
                computerEditModel.computer.PSU_ID == 0 || computerEditModel.computer.Cooler_ID == 0)
            {
                check = false;
                ModelState.AddModelError("DetalErr", "Nevisi laukeliai pilnai uzpildyti");
                return check;
            }

            Product_CPU CPU = db.Product_CPU.Where(x => x.ID == computerEditModel.computer.CPU_ID).FirstOrDefault();
            Product_MotherBoard motherboard = db.Product_MotherBoard.Where(x => x.ID == computerEditModel.computer.MB_ID).FirstOrDefault();
            Product_RAM ram = db.Product_RAM.Where(x => x.ID == computerEditModel.computer.Ram_ID).FirstOrDefault();


            if (!motherboard.CPU_Socket.Contains(CPU.Generation))
            {
                ModelState.AddModelError("CPUErr", "Procesorius netinka jūsų pasirinktai motininei plokštei");
                check = false;
            }



            if (ram.Type.Contains("DDR3"))
            {
                if (!motherboard.RAM_Compability.Contains("DDR3"))
                {
                    check = false;
                    ModelState.AddModelError("RAMErr", "RAM atmintis netinka jūsų pasirinktai motininei plokštei");
                }
            }

            if (ram.Type.Contains("DDR4"))
            {
                if (!motherboard.RAM_Compability.Contains("DDR4"))
                {
                    check = false;
                    ModelState.AddModelError("RAMErr", "RAM atmintis netinka jūsų pasirinktai motininei plokštei");
                }
            }



            return check;
        }


    }
}