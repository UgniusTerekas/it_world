﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITWorld.Models;

namespace ITWorld.ModelView
{
    public class ShopingCart
    {
        public Product product { get; set; }
        public int kiekis { get; set; }
        public int productID { get; set; }
    }
}